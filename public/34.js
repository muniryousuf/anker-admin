(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/AddChoice.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/AddChoice.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ModalBox__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ModalBox */ "./resources/js/components/ModalBox.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "AddChoice",
  components: {
    ModalBox: _ModalBox__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: ['isActive', 'groupId', 'groupName'],
  data: function data() {
    return {
      isModalActive: false,
      preselect: false,
      form: {
        name: null,
        price: null,
        preselect: null,
        id_group: null
      }
    };
  },
  methods: {
    cancel: function cancel() {
      this.$emit('cancel');
    },
    addChoice: function addChoice() {
      var _this = this;

      this.form.id_group = this.groupId;
      this.form.preselect = this.preselect;
      var method = 'post';
      var url = '/choices/store';
      axios({
        method: method,
        url: url,
        data: this.form
      }).then(function (r) {
        if (!_this.id && r.data.data.id) {
          _this.cancel();

          _this.$emit('call-list');

          _this.$buefy.snackbar.open({
            message: 'Created',
            queue: false
          });
        }
      })["catch"](function (e) {
        _this.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    }
  },
  watch: {
    isActive: function isActive(newValue) {
      this.isModalActive = newValue;
    },
    isModalActive: function isModalActive(newValue) {
      if (!newValue) {
        this.cancel();
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_CardComponent__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/CardComponent */ "./resources/js/components/CardComponent.vue");
/* harmony import */ var vue_bulma_accordion__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-bulma-accordion */ "./node_modules/vue-bulma-accordion/dist/main.js");
/* harmony import */ var vue_bulma_accordion__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_bulma_accordion__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_AddProduct__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/AddProduct */ "./resources/js/components/AddProduct.vue");
/* harmony import */ var _components_AddChoice__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/AddChoice */ "./resources/js/components/AddChoice.vue");
/* harmony import */ var _components_ModalTrashBox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/ModalTrashBox */ "./resources/js/components/ModalTrashBox.vue");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vuedraggable */ "./node_modules/vuedraggable/dist/vuedraggable.common.js");
/* harmony import */ var vuedraggable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vuedraggable__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var buefy_src_components_radio_Radio__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! buefy/src/components/radio/Radio */ "./node_modules/buefy/src/components/radio/Radio.vue");
/* harmony import */ var buefy_src_components_radio_RadioButton__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! buefy/src/components/radio/RadioButton */ "./node_modules/buefy/src/components/radio/RadioButton.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Menu Setup',
  components: {
    BRadioButton: buefy_src_components_radio_RadioButton__WEBPACK_IMPORTED_MODULE_7__["default"],
    BRadio: buefy_src_components_radio_Radio__WEBPACK_IMPORTED_MODULE_6__["default"],
    ModalTrashBox: _components_ModalTrashBox__WEBPACK_IMPORTED_MODULE_4__["default"],
    AddProduct: _components_AddProduct__WEBPACK_IMPORTED_MODULE_2__["default"],
    AddChoice: _components_AddChoice__WEBPACK_IMPORTED_MODULE_3__["default"],
    draggable: vuedraggable__WEBPACK_IMPORTED_MODULE_5___default.a,
    CardComponent: _components_CardComponent__WEBPACK_IMPORTED_MODULE_0__["default"],
    BulmaAccordion: vue_bulma_accordion__WEBPACK_IMPORTED_MODULE_1__["BulmaAccordion"],
    BulmaAccordionItem: vue_bulma_accordion__WEBPACK_IMPORTED_MODULE_1__["BulmaAccordionItem"]
  },
  data: function data() {
    return {
      isLoading: false,
      errors: {},
      categories: [],
      addons: [],
      categorySection: false,
      groupSection: false,
      selectedCategoryId: null,
      selectedCategoryName: null,
      selectedGroupId: null,
      selectedGroupName: null,
      showAddProductModal: false,
      showAddChoicesModal: false,
      groupRadio: 'Optional',
      groupSingle: 'Single',
      form: {
        name: null,
        description: null,
        status: 0,
        image: null
      },
      groupform: {
        name: null,
        type: null
      }
    };
  },
  methods: {
    submit: function submit() {
      var _this = this;

      var config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      };
      var formData = new FormData();
      formData.append('name', this.form.name);
      formData.append('description', this.form.description);
      formData.append('status', this.form.status);
      formData.append('image', this.form.image);
      var method = 'post';
      var url = '/categories/store';
      axios({
        method: method,
        url: url,
        data: formData,
        config: config
      }).then(function (r) {
        _this.isLoading = false;

        if (!_this.id && r.data.data.id) {
          _this.categorySection = false;

          _this.getData();

          _this.form.name = null;
          _this.form.status = 0;
          _this.form.description = null;
          _this.form.image = null;

          _this.$buefy.snackbar.open({
            message: 'Category Added Successfully',
            queue: false
          });
        }
      })["catch"](function (e) {
        _this.isLoading = false;

        _this.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    submitGroup: function submitGroup() {
      var _this2 = this;

      this.groupform.type = this.groupRadio;
      this.groupform.display_type = this.groupSingle;
      var method = 'post';
      var url = '/choicesGroup/store';
      axios({
        method: method,
        url: url,
        data: this.groupform
      }).then(function (r) {
        _this2.isLoading = false;

        if (!_this2.id && r.data.data.id) {
          _this2.groupSection = false;
          _this2.groupform.name = null;

          _this2.getAddons();

          _this2.$buefy.snackbar.open({
            message: 'Created',
            queue: false
          });
        }
      })["catch"](function (e) {
        _this2.isLoading = false;

        _this2.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    getData: function getData() {
      var _this3 = this;

      var self = this;
      axios.get('/categories').then(function (r) {
        _this3.isLoading = false;

        if (r.data && r.data.data) {
          if (r.data.data.length > _this3.perPage) {
            _this3.paginated = true;
          }

          self.categories = r.data.data;
        }
      })["catch"](function (err) {
        _this3.isLoading = false;

        _this3.$buefy.toast.open({
          message: "Error: ".concat(err.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    getAddons: function getAddons() {
      var _this4 = this;

      var self = this;
      axios.get('/choicesGroup').then(function (r) {
        _this4.isLoading = false;

        if (r.data && r.data.data) {
          if (r.data.data.length > _this4.perPage) {
            _this4.paginated = true;
          }

          self.addons = r.data.data;
        }
      })["catch"](function (err) {
        _this4.isLoading = false;

        _this4.$buefy.toast.open({
          message: "Error: ".concat(err.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    removeProductGroup: function removeProductGroup(productId, groupId) {
      var _this5 = this;

      var method = 'post';
      var url = '/productGroups/destroy';
      axios({
        method: method,
        url: url,
        data: {
          id_group: groupId,
          id_product: productId
        }
      }).then(function (r) {
        _this5.isLoading = false;

        _this5.getData();
      })["catch"](function (e) {
        _this5.isLoading = false;

        _this5.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    removeCategory: function removeCategory(categoryId) {
      var _this6 = this;

      var method = 'delete';
      var url = '/categories/' + categoryId + '/destroy';
      axios({
        method: method,
        url: url
      }).then(function (r) {
        _this6.isLoading = false;

        _this6.getData();
      })["catch"](function (e) {
        _this6.isLoading = false;

        _this6.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    removeProduct: function removeProduct(productId) {
      var _this7 = this;

      var method = 'delete';
      var url = '/products/' + productId + '/destroy';
      axios({
        method: method,
        url: url
      }).then(function (r) {
        _this7.isLoading = false;

        _this7.getData();
      })["catch"](function (e) {
        _this7.isLoading = false;

        _this7.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    showCategorySection: function showCategorySection() {
      var self = this;

      if (self.categorySection == false) {
        self.categorySection = true;
      } else {
        self.categorySection = false;
      }
    },
    showGroupSection: function showGroupSection() {
      var self = this;

      if (self.groupSection == false) {
        self.groupSection = true;
      } else {
        self.groupSection = false;
      }
    },
    addProductModal: function addProductModal(id, name) {
      this.showAddProductModal = true;
      this.selectedCategoryId = id;
      this.selectedCategoryName = name;
    },
    hideModal: function hideModal() {
      this.showAddProductModal = false;
      this.selectedCategoryId = null;
      this.selectedCategoryName = null;
    },
    addChoicesModal: function addChoicesModal(id, name) {
      this.showAddChoicesModal = true;
      this.selectedGroupId = id;
      this.selectedGroupName = name;
    },
    hideChoicesModal: function hideChoicesModal() {
      this.showAddChoicesModal = false;
      this.selectedGroupId = null;
      this.selectedGroupName = null;
    },
    onCallList: function onCallList() {
      this.onApply();
    },
    onApply: function onApply() {
      this.getData();
      this.getAddons();
    },
    onAdd: function onAdd(evt) {
      var _this8 = this;

      var groupId = evt.item.getAttribute('groupid');
      var ProductId = evt.to.firstChild.getAttribute('listId');
      var method = 'post';
      var url = '/productGroups/store';
      axios({
        method: method,
        url: url,
        data: {
          id_group: groupId,
          id_product: ProductId
        }
      }).then(function (r) {
        _this8.isLoading = false;

        if (r.data.data.id) {
          _this8.getData();

          evt.to.innerHTML = "<li listId=" + ProductId + "></li>";

          _this8.$buefy.snackbar.open({
            message: 'Created',
            queue: false
          });
        }
      })["catch"](function (e) {
        _this8.isLoading = false;

        _this8.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    }
  },
  computed: {
    formNameType: function formNameType() {
      return this.errors.name ? 'is-danger' : null;
    },
    formNameMessage: function formNameMessage() {
      return this.errors.name ? this.errors.name[0] : 'Required. Category Name';
    },
    formDescriptionType: function formDescriptionType() {
      return this.errors.description ? 'is-danger' : null;
    },
    formDescriptionMessage: function formDescriptionMessage() {
      return this.errors.description ? this.errors.description[0] : 'Required. Category Description';
    },
    myList: {
      get: function get() {
        return this.$store.state.myList;
      },
      set: function set(value) {
        this.$store.commit('updateList', value);
      }
    }
  },
  created: function created() {
    this.getData();
    this.getAddons();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/AddChoice.vue?vue&type=template&id=4ed80ec7&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/AddChoice.vue?vue&type=template&id=4ed80ec7& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "b-modal",
    {
      attrs: { active: _vm.isModalActive, "has-modal-card": "" },
      on: {
        "update:active": function($event) {
          _vm.isModalActive = $event
        }
      }
    },
    [
      _c("div", { staticClass: "modal-card" }, [
        _c("header", { staticClass: "modal-card-head" }, [
          _c("p", { staticClass: "modal-card-title" }, [
            _vm._v("Add choice of " + _vm._s(_vm.groupName))
          ])
        ]),
        _vm._v(" "),
        _c(
          "section",
          { staticClass: "modal-card-body" },
          [
            _c(
              "b-field",
              { attrs: { horizontal: "", label: "Name" } },
              [
                _c("b-input", {
                  attrs: { name: "name", required: "" },
                  model: {
                    value: _vm.form.name,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "name", $$v)
                    },
                    expression: "form.name"
                  }
                })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-field",
              { attrs: { horizontal: "", label: "Price" } },
              [
                _c("b-input", {
                  attrs: { name: "price", required: "" },
                  model: {
                    value: _vm.form.price,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "price", $$v)
                    },
                    expression: "form.price"
                  }
                })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-field",
              { attrs: { horizontal: "", label: "pre-select" } },
              [
                _c("b-switch", {
                  model: {
                    value: _vm.preselect,
                    callback: function($$v) {
                      _vm.preselect = $$v
                    },
                    expression: "preselect"
                  }
                })
              ],
              1
            ),
            _vm._v(" "),
            _vm._t("default")
          ],
          2
        ),
        _vm._v(" "),
        _c("footer", { staticClass: "modal-card-foot" }, [
          _c(
            "button",
            {
              staticClass: "button",
              attrs: { type: "button" },
              on: { click: _vm.cancel }
            },
            [_vm._v("Cancel")]
          ),
          _vm._v(" "),
          _c(
            "button",
            {
              staticClass: "button primary",
              on: {
                click: function($event) {
                  return _vm.addChoice()
                }
              }
            },
            [_vm._v("Submit")]
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=template&id=68853ffe&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=template&id=68853ffe& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "section",
      { staticClass: "section is-main-section" },
      [
        _c(
          "card-component",
          { attrs: { title: "Menu", icon: "account-circle" } },
          [
            _vm.categories.length
              ? _c(
                  "BulmaAccordion",
                  { attrs: { dropdown: "", icon: "caret" } },
                  _vm._l(_vm.categories, function(cat, key) {
                    return _c("BulmaAccordionItem", [
                      _c("h4", { attrs: { slot: "title" }, slot: "title" }, [
                        _vm._v(_vm._s(cat.name))
                      ]),
                      _vm._v(" "),
                      _c(
                        "p",
                        { attrs: { slot: "content" }, slot: "content" },
                        [
                          _vm._l(cat.products, function(product) {
                            return cat.products
                              ? _c(
                                  "card-component",
                                  {
                                    staticClass: "tile is-child",
                                    staticStyle: {
                                      "margin-bottom": "20px !important"
                                    },
                                    attrs: { icon: "account-circle" }
                                  },
                                  [
                                    _c("h3", [_vm._v(_vm._s(product.name))]),
                                    _vm._v(" "),
                                    _c(
                                      "h6",
                                      {
                                        staticStyle: {
                                          float: "right",
                                          "font-weight": "bold"
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "£ " + _vm._s(product.price) + "/="
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(product.product_groups, function(
                                      productgroups
                                    ) {
                                      return product.product_groups
                                        ? _c(
                                            "card-component",
                                            {
                                              staticClass: "tile is-child",
                                              staticStyle: {
                                                "margin-top": "20px !important",
                                                width: "25% !important"
                                              },
                                              attrs: { icon: "account-circle" }
                                            },
                                            [
                                              _c("h3", [
                                                _vm._v(
                                                  _vm._s(
                                                    productgroups.group.name
                                                  )
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("b-button", {
                                                attrs: {
                                                  "icon-right": "delete"
                                                },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.removeProductGroup(
                                                      product.id,
                                                      productgroups.group.id
                                                    )
                                                  }
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        : _vm._e()
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "draggable",
                                      {
                                        staticStyle: { height: "25px" },
                                        attrs: {
                                          tag: "ul",
                                          options: {
                                            group: {
                                              name: "Items",
                                              pull: false,
                                              put: true
                                            }
                                          }
                                        },
                                        on: { add: _vm.onAdd }
                                      },
                                      [
                                        _c("li", {
                                          attrs: { listId: product.id }
                                        })
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("b-button", {
                                      attrs: { "icon-right": "delete" },
                                      on: {
                                        click: function($event) {
                                          return _vm.removeProduct(product.id)
                                        }
                                      }
                                    })
                                  ],
                                  2
                                )
                              : _vm._e()
                          }),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              staticClass: "button is-black is-outlined",
                              attrs: { href: "javascript:;" },
                              on: {
                                click: function($event) {
                                  return _vm.addProductModal(cat.id, cat.name)
                                }
                              }
                            },
                            [_vm._v(" Add Item to " + _vm._s(cat.name))]
                          ),
                          _vm._v(" "),
                          _c("b-button", {
                            attrs: { "icon-right": "delete" },
                            on: {
                              click: function($event) {
                                return _vm.removeCategory(cat.id)
                              }
                            }
                          })
                        ],
                        2
                      )
                    ])
                  }),
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _c("add-product", {
              attrs: {
                "is-active": _vm.showAddProductModal,
                categoryId: _vm.selectedCategoryId,
                categoryName: _vm.selectedCategoryName
              },
              on: { cancel: _vm.hideModal, "call-list": _vm.onCallList }
            }),
            _vm._v(" "),
            _c("add-choice", {
              attrs: {
                "is-active": _vm.showAddChoicesModal,
                groupId: _vm.selectedGroupId,
                groupName: _vm.selectedGroupName
              },
              on: { cancel: _vm.hideChoicesModal, "call-list": _vm.onCallList }
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "card-component",
          { attrs: { title: "Choices & Addons", icon: "account-circle" } },
          [
            _vm.addons.length
              ? _c(
                  "BulmaAccordion",
                  { attrs: { dropdown: "", icon: "caret" } },
                  [
                    _c(
                      "draggable",
                      {
                        attrs: {
                          options: {
                            group: { name: "Items", pull: "clone", put: false }
                          }
                        },
                        on: { add: _vm.onAdd }
                      },
                      _vm._l(_vm.addons, function(addon, key) {
                        return _c(
                          "BulmaAccordionItem",
                          { attrs: { groupId: addon.id } },
                          [
                            _c(
                              "h4",
                              { attrs: { slot: "title" }, slot: "title" },
                              [_vm._v(_vm._s(addon.name))]
                            ),
                            _vm._v(" "),
                            _c(
                              "p",
                              { attrs: { slot: "content" }, slot: "content" },
                              [
                                _vm._l(addon.choices, function(choice) {
                                  return addon.choices
                                    ? _c(
                                        "card-component",
                                        {
                                          staticClass: "tile is-child",
                                          staticStyle: {
                                            "margin-bottom": "20px !important"
                                          },
                                          attrs: { icon: "account-circle" }
                                        },
                                        [
                                          _c("h3", [
                                            _vm._v(_vm._s(choice.name))
                                          ]),
                                          _vm._v(" "),
                                          _c(
                                            "h6",
                                            {
                                              staticStyle: {
                                                float: "right",
                                                "font-weight": "bold"
                                              }
                                            },
                                            [
                                              _vm._v(
                                                "£. " +
                                                  _vm._s(choice.price) +
                                                  "/="
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                }),
                                _vm._v(" "),
                                _c("br"),
                                _vm._v(" "),
                                _c(
                                  "a",
                                  {
                                    staticClass: "button is-black is-outlined",
                                    attrs: { href: "javascript:;" },
                                    on: {
                                      click: function($event) {
                                        return _vm.addChoicesModal(
                                          addon.id,
                                          addon.name
                                        )
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      " Add choice of " + _vm._s(addon.name)
                                    )
                                  ]
                                )
                              ],
                              2
                            )
                          ]
                        )
                      }),
                      1
                    )
                  ],
                  1
                )
              : _vm._e(),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "button is-black is-outlined",
                attrs: { href: "javascript:;" },
                on: {
                  click: function($event) {
                    return _vm.showGroupSection()
                  }
                }
              },
              [_vm._v(" Add Group ")]
            ),
            _vm._v(" "),
            _c("br"),
            _c("br"),
            _vm._v(" "),
            _vm.groupSection
              ? _c(
                  "card-component",
                  { attrs: { title: "Add Group", icon: "account-circle" } },
                  [
                    _c(
                      "form",
                      {
                        on: {
                          submit: function($event) {
                            $event.preventDefault()
                            return _vm.submitGroup($event)
                          }
                        }
                      },
                      [
                        _c(
                          "b-field",
                          { attrs: { horizontal: "", label: "Name" } },
                          [
                            _c("b-input", {
                              attrs: { name: "name", required: "" },
                              model: {
                                value: _vm.groupform.name,
                                callback: function($$v) {
                                  _vm.$set(_vm.groupform, "name", $$v)
                                },
                                expression: "groupform.name"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "b-field",
                          {
                            attrs: {
                              horizontal: "",
                              label: "Group Type",
                              required: ""
                            }
                          },
                          [
                            _c(
                              "b-radio",
                              {
                                attrs: {
                                  name: "type",
                                  "native-value": "Optional"
                                },
                                model: {
                                  value: _vm.groupRadio,
                                  callback: function($$v) {
                                    _vm.groupRadio = $$v
                                  },
                                  expression: "groupRadio"
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                Optional\n                            "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "b-radio",
                              {
                                attrs: {
                                  name: "type",
                                  "native-value": "Mandatory"
                                },
                                model: {
                                  value: _vm.groupRadio,
                                  callback: function($$v) {
                                    _vm.groupRadio = $$v
                                  },
                                  expression: "groupRadio"
                                }
                              },
                              [
                                _vm._v(
                                  "\n                                Mandatory\n                            "
                                )
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "b-field",
                          {
                            attrs: {
                              horizontal: "",
                              label: "Single or Multiple",
                              required: ""
                            }
                          },
                          [
                            _c(
                              "b-radio",
                              {
                                attrs: {
                                  name: "displaytype",
                                  "native-value": "Single"
                                },
                                model: {
                                  value: _vm.groupSingle,
                                  callback: function($$v) {
                                    _vm.groupSingle = $$v
                                  },
                                  expression: "groupSingle"
                                }
                              },
                              [
                                _vm._v(
                                  "\n                            Single\n                          "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "b-radio",
                              {
                                attrs: {
                                  name: "displaytype",
                                  "native-value": "Multiple"
                                },
                                model: {
                                  value: _vm.groupSingle,
                                  callback: function($$v) {
                                    _vm.groupSingle = $$v
                                  },
                                  expression: "groupSingle"
                                }
                              },
                              [
                                _vm._v(
                                  "\n                            Multiple\n                          "
                                )
                              ]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("hr"),
                        _vm._v(" "),
                        _c("b-field", { attrs: { horizontal: "" } }, [
                          _c("div", { staticClass: "buttons" }, [
                            _c(
                              "a",
                              {
                                staticClass: "button is-black is-outlined",
                                attrs: { href: "javascript:;" },
                                on: {
                                  click: function($event) {
                                    return _vm.showGroupSection()
                                  }
                                }
                              },
                              [_vm._v("Cancel")]
                            ),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "button is-black",
                                class: { "is-loading": _vm.isLoading },
                                attrs: { type: "submit" }
                              },
                              [
                                _vm._v(
                                  "\n                                    Submit\n                                "
                                )
                              ]
                            )
                          ])
                        ])
                      ],
                      1
                    )
                  ]
                )
              : _vm._e()
          ],
          1
        ),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c(
          "a",
          {
            staticClass: "button is-black is-outlined",
            attrs: { href: "javascript:;" },
            on: {
              click: function($event) {
                return _vm.showCategorySection()
              }
            }
          },
          [_vm._v(" Add Category ")]
        ),
        _vm._v(" "),
        _c("br"),
        _c("br"),
        _vm._v(" "),
        _vm.categorySection
          ? _c(
              "card-component",
              { attrs: { title: "Add Category", icon: "account-circle" } },
              [
                _c(
                  "form",
                  {
                    attrs: { enctype: "multipart/form-data" },
                    on: {
                      submit: function($event) {
                        $event.preventDefault()
                        return _vm.submit($event)
                      }
                    }
                  },
                  [
                    _c(
                      "b-field",
                      {
                        attrs: {
                          horizontal: "",
                          label: "Name",
                          message: _vm.formNameMessage,
                          type: _vm.formNameType
                        }
                      },
                      [
                        _c("b-input", {
                          attrs: { name: "name", required: "" },
                          model: {
                            value: _vm.form.name,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "name", $$v)
                            },
                            expression: "form.name"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-field",
                      {
                        attrs: {
                          horizontal: "",
                          label: "Description",
                          message: _vm.formDescriptionMessage,
                          type: _vm.formDescriptionType
                        }
                      },
                      [
                        _c("b-input", {
                          attrs: { name: "description", required: "" },
                          model: {
                            value: _vm.form.description,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "description", $$v)
                            },
                            expression: "form.description"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-field",
                      { attrs: { horizontal: "", label: "Status" } },
                      [
                        _c("b-checkbox", {
                          attrs: { "true-value": "1", "false-value": "0" },
                          model: {
                            value: _vm.form.status,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "status", $$v)
                            },
                            expression: "form.status"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "b-field",
                      { attrs: { horizontal: "" } },
                      [
                        _c(
                          "b-upload",
                          {
                            attrs: { accept: ".jpg,.png" },
                            model: {
                              value: _vm.form.image,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "image", $$v)
                              },
                              expression: "form.image"
                            }
                          },
                          [
                            _c(
                              "a",
                              { staticClass: "button is-primary" },
                              [
                                _c("b-icon", { attrs: { icon: "upload" } }),
                                _vm._v(" "),
                                _c("span", [_vm._v("Upload Feature Image")])
                              ],
                              1
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _vm.form.image
                          ? _c("span", { staticClass: "file-name" }, [
                              _vm._v(
                                "\n                    " +
                                  _vm._s(_vm.form.image.name) +
                                  "\n                "
                              )
                            ])
                          : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("b-field", { attrs: { horizontal: "" } }, [
                      _c("div", { staticClass: "buttons" }, [
                        _c(
                          "a",
                          {
                            staticClass: "button is-black is-outlined",
                            attrs: { href: "javascript:;" },
                            on: {
                              click: function($event) {
                                return _vm.showCategorySection()
                              }
                            }
                          },
                          [_vm._v("Cancel")]
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "button is-black",
                            class: { "is-loading": _vm.isLoading },
                            attrs: { type: "submit" }
                          },
                          [
                            _vm._v(
                              "\n                            Submit\n                        "
                            )
                          ]
                        )
                      ])
                    ])
                  ],
                  1
                )
              ]
            )
          : _vm._e()
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/AddChoice.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/AddChoice.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AddChoice_vue_vue_type_template_id_4ed80ec7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddChoice.vue?vue&type=template&id=4ed80ec7& */ "./resources/js/components/AddChoice.vue?vue&type=template&id=4ed80ec7&");
/* harmony import */ var _AddChoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddChoice.vue?vue&type=script&lang=js& */ "./resources/js/components/AddChoice.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _AddChoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AddChoice_vue_vue_type_template_id_4ed80ec7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _AddChoice_vue_vue_type_template_id_4ed80ec7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/AddChoice.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/AddChoice.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/AddChoice.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddChoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./AddChoice.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/AddChoice.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddChoice_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/AddChoice.vue?vue&type=template&id=4ed80ec7&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/AddChoice.vue?vue&type=template&id=4ed80ec7& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddChoice_vue_vue_type_template_id_4ed80ec7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./AddChoice.vue?vue&type=template&id=4ed80ec7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/AddChoice.vue?vue&type=template&id=4ed80ec7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddChoice_vue_vue_type_template_id_4ed80ec7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddChoice_vue_vue_type_template_id_4ed80ec7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/MenuSetup/MenuSetup.vue":
/*!****************************************************!*\
  !*** ./resources/js/views/MenuSetup/MenuSetup.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _MenuSetup_vue_vue_type_template_id_68853ffe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./MenuSetup.vue?vue&type=template&id=68853ffe& */ "./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=template&id=68853ffe&");
/* harmony import */ var _MenuSetup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MenuSetup.vue?vue&type=script&lang=js& */ "./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _MenuSetup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _MenuSetup_vue_vue_type_template_id_68853ffe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _MenuSetup_vue_vue_type_template_id_68853ffe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/MenuSetup/MenuSetup.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuSetup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./MenuSetup.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuSetup_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=template&id=68853ffe&":
/*!***********************************************************************************!*\
  !*** ./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=template&id=68853ffe& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuSetup_vue_vue_type_template_id_68853ffe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./MenuSetup.vue?vue&type=template&id=68853ffe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/MenuSetup/MenuSetup.vue?vue&type=template&id=68853ffe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuSetup_vue_vue_type_template_id_68853ffe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MenuSetup_vue_vue_type_template_id_68853ffe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);