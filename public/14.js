(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[14],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Deals/DealsForm.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Deals/DealsForm.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_TitleBar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/TitleBar */ "./resources/js/components/TitleBar.vue");
/* harmony import */ var _components_HeroBar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/HeroBar */ "./resources/js/components/HeroBar.vue");
/* harmony import */ var _components_Tiles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/Tiles */ "./resources/js/components/Tiles.vue");
/* harmony import */ var _components_CardComponent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/CardComponent */ "./resources/js/components/CardComponent.vue");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-multiselect */ "./node_modules/vue-multiselect/dist/vue-multiselect.min.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_multiselect__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'DealForm',
  components: {
    CardComponent: _components_CardComponent__WEBPACK_IMPORTED_MODULE_3__["default"],
    Tiles: _components_Tiles__WEBPACK_IMPORTED_MODULE_2__["default"],
    HeroBar: _components_HeroBar__WEBPACK_IMPORTED_MODULE_1__["default"],
    TitleBar: _components_TitleBar__WEBPACK_IMPORTED_MODULE_0__["default"],
    Multiselect: vue_multiselect__WEBPACK_IMPORTED_MODULE_4___default.a
  },
  props: {
    id: {
      "default": null
    }
  },
  data: function data() {
    return {
      isLoading: false,
      item: null,
      form: this.getClearFormObject(),
      options: []
    };
  },
  computed: {
    titleStack: function titleStack() {
      var lastCrumb;

      if (this.id) {
        lastCrumb = this.form.name;
      } else {
        lastCrumb = 'New Deal';
      }

      return ['Admin', 'Deals', lastCrumb];
    },
    heroTitle: function heroTitle() {
      if (this.id) {
        return this.form.name;
      } else {
        return 'Create Deal';
      }
    },
    formCardTitle: function formCardTitle() {
      if (this.id) {
        return 'Edit Deal';
      } else {
        return 'New Deal';
      }
    },
    isProfileExists: function isProfileExists() {
      return !!this.item;
    }
  },
  created: function created() {
    this.getData();
    this.getProducts();
  },
  methods: {
    getClearFormObject: function getClearFormObject() {
      return {
        id: null,
        name: null,
        price: null,
        status: 0,
        description: null,
        products: []
      };
    },
    getData: function getData() {
      var _this = this;

      if (this.id) {
        axios.get("/deals/".concat(this.id)).then(function (r) {
          _this.form = r.data.data;
          var arr = [];
          var results = r.data.data.products;
          results.forEach(function (item) {
            var arr2 = {
              item_id: item.item_id,
              quantity: item.quantity,
              is_category: item.is_category,
              is_options: item.is_options
            };
            arr.push(arr2);
          });
          console.log(arr);
          _this.form.products = arr;
        })["catch"](function (e) {
          _this.item = null;

          _this.$buefy.toast.open({
            message: "Error: ".concat(e.message),
            type: 'is-danger',
            queue: false
          });
        });
      }
    },
    getProducts: function getProducts() {
      var _this2 = this;

      axios.get("deals-products").then(function (r) {
        if (r.data && r.data.data) {
          var results = r.data.data;
          _this2.options = results;
        }
      })["catch"](function (e) {
        _this2.options = null;

        _this2.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    submit: function submit() {
      var _this3 = this;

      this.isLoading = true;
      var method = 'post';
      var url = '/deals/store';

      if (this.id) {
        method = 'patch';
        url = "/deals/".concat(this.id);
      }

      axios({
        method: method,
        url: url,
        data: this.form
      }).then(function (r) {
        _this3.isLoading = false;

        _this3.$router.push({
          name: 'deals.index'
        });

        if (!_this3.id && r.data.data.id) {
          _this3.$buefy.snackbar.open({
            message: 'Deal Created Successfully',
            queue: false
          });
        } else {
          _this3.item = r.data.data;

          _this3.$buefy.snackbar.open({
            message: 'Deal Updated Successfully',
            queue: false
          });
        }
      })["catch"](function (e) {
        _this3.isLoading = false;

        _this3.$buefy.toast.open({
          message: "Error: ".concat(e.message),
          type: 'is-danger',
          queue: false
        });
      });
    },
    addItem: function addItem() {
      this.form.products.push({
        item_id: null,
        quantity: 0,
        is_category: 0,
        is_options: 0
      });
    },
    deleteItem: function deleteItem(index) {
      this.form.products.splice(index, 1);
    }
  },
  watch: {
    id: function id(newValue) {
      this.form = this.getClearFormObject();
      this.item = null;

      if (newValue) {
        this.getData();
        this.getProducts();
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Deals/DealsForm.vue?vue&type=template&id=d182fc18&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Deals/DealsForm.vue?vue&type=template&id=d182fc18& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("title-bar", { attrs: { "title-stack": _vm.titleStack } }),
      _vm._v(" "),
      _c(
        "hero-bar",
        [
          _vm._v("\n    " + _vm._s(_vm.heroTitle) + "\n    "),
          _c(
            "router-link",
            {
              staticClass: "button",
              attrs: { slot: "right", to: "/deals/index" },
              slot: "right"
            },
            [_vm._v("\n      Deals\n    ")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "section",
        { staticClass: "section is-main-section" },
        [
          _c(
            "tiles",
            [
              _c(
                "card-component",
                {
                  staticClass: "tile is-child",
                  attrs: { title: _vm.formCardTitle, icon: "account-edit" }
                },
                [
                  _c(
                    "form",
                    {
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.submit($event)
                        }
                      }
                    },
                    [
                      _vm.id
                        ? [
                            _c(
                              "b-field",
                              { attrs: { label: "ID", horizontal: "" } },
                              [
                                _c("b-input", {
                                  attrs: {
                                    value: _vm.id,
                                    "custom-class": "is-static",
                                    readonly: ""
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("hr")
                          ]
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "b-field",
                        { attrs: { label: "Deal Name", horizontal: "" } },
                        [
                          _c("b-input", {
                            attrs: { placeholder: "eg. Deal 1", required: "" },
                            model: {
                              value: _vm.form.name,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "name", $$v)
                              },
                              expression: "form.name"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-field",
                        { attrs: { label: "Description", horizontal: "" } },
                        [
                          _c("b-input", {
                            attrs: { placeholder: "eg. Deal 1", required: "" },
                            model: {
                              value: _vm.form.description,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "description", $$v)
                              },
                              expression: "form.description"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-field",
                        { attrs: { label: "Deal Price", horizontal: "" } },
                        [
                          _c("b-input", {
                            attrs: { placeholder: "eg. 2500", required: "" },
                            model: {
                              value: _vm.form.price,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "price", $$v)
                              },
                              expression: "form.price"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm._l(_vm.form.products, function(input, index) {
                        return _c(
                          "span",
                          [
                            _c(
                              "b-field",
                              {
                                attrs: {
                                  horizontal: "",
                                  label: "Select Deal Item"
                                }
                              },
                              [
                                _c(
                                  "b-select",
                                  {
                                    attrs: {
                                      placeholder: "Select Item",
                                      name: "item_id[index]",
                                      required: ""
                                    },
                                    model: {
                                      value: input.item_id,
                                      callback: function($$v) {
                                        _vm.$set(input, "item_id", $$v)
                                      },
                                      expression: "input.item_id"
                                    }
                                  },
                                  _vm._l(_vm.options, function(option) {
                                    return _c(
                                      "option",
                                      {
                                        key: option.id,
                                        domProps: { value: option.id }
                                      },
                                      [
                                        _vm._v(
                                          "\n                  " +
                                            _vm._s(option.name) +
                                            "\n              "
                                        )
                                      ]
                                    )
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _c(
                                  "b-field",
                                  {
                                    attrs: { horizontal: "", label: "Category" }
                                  },
                                  [
                                    _c("b-checkbox", {
                                      attrs: {
                                        name: "is_category['index']",
                                        "true-value": "1",
                                        "false-value": "0"
                                      },
                                      model: {
                                        value: input.is_category,
                                        callback: function($$v) {
                                          _vm.$set(input, "is_category", $$v)
                                        },
                                        expression: "input.is_category"
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "b-field",
                                  {
                                    attrs: {
                                      horizontal: "",
                                      label: "Allo Options"
                                    }
                                  },
                                  [
                                    _c("b-checkbox", {
                                      attrs: {
                                        name: "is_options['index']",
                                        "true-value": "1",
                                        "false-value": "0"
                                      },
                                      model: {
                                        value: input.is_options,
                                        callback: function($$v) {
                                          _vm.$set(input, "is_options", $$v)
                                        },
                                        expression: "input.is_options"
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "button",
                                  {
                                    staticClass: "button",
                                    attrs: { type: "button" },
                                    on: {
                                      click: function($event) {
                                        return _vm.deleteItem(index)
                                      }
                                    }
                                  },
                                  [_vm._v("Remove")]
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "b-field",
                              {
                                attrs: {
                                  horizontal: "",
                                  label: "Item Quantity"
                                }
                              },
                              [
                                _c("b-input", {
                                  attrs: {
                                    name: "quantity['index']",
                                    placeholder: "Quantity",
                                    required: ""
                                  },
                                  model: {
                                    value: input.quantity,
                                    callback: function($$v) {
                                      _vm.$set(input, "quantity", $$v)
                                    },
                                    expression: "input.quantity"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("hr")
                          ],
                          1
                        )
                      }),
                      _vm._v(" "),
                      _c("b-field", { attrs: { horizontal: "" } }, [
                        _c(
                          "button",
                          {
                            staticClass: "button",
                            attrs: { type: "button" },
                            on: { click: _vm.addItem }
                          },
                          [_vm._v("Add Item")]
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "b-field",
                        { attrs: { horizontal: "", label: "Status" } },
                        [
                          _c("b-checkbox", {
                            attrs: { "true-value": "1", "false-value": "0" },
                            model: {
                              value: _vm.form.status,
                              callback: function($$v) {
                                _vm.$set(_vm.form, "status", $$v)
                              },
                              expression: "form.status"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c(
                        "b-field",
                        { attrs: { horizontal: "" } },
                        [
                          _c(
                            "b-button",
                            {
                              attrs: {
                                type: "is-primary",
                                loading: _vm.isLoading,
                                "native-type": "submit"
                              }
                            },
                            [_vm._v("Submit")]
                          )
                        ],
                        1
                      )
                    ],
                    2
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Deals/DealsForm.vue":
/*!************************************************!*\
  !*** ./resources/js/views/Deals/DealsForm.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DealsForm_vue_vue_type_template_id_d182fc18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DealsForm.vue?vue&type=template&id=d182fc18& */ "./resources/js/views/Deals/DealsForm.vue?vue&type=template&id=d182fc18&");
/* harmony import */ var _DealsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DealsForm.vue?vue&type=script&lang=js& */ "./resources/js/views/Deals/DealsForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DealsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DealsForm_vue_vue_type_template_id_d182fc18___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DealsForm_vue_vue_type_template_id_d182fc18___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Deals/DealsForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Deals/DealsForm.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/Deals/DealsForm.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DealsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./DealsForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Deals/DealsForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DealsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Deals/DealsForm.vue?vue&type=template&id=d182fc18&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/Deals/DealsForm.vue?vue&type=template&id=d182fc18& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DealsForm_vue_vue_type_template_id_d182fc18___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./DealsForm.vue?vue&type=template&id=d182fc18& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Deals/DealsForm.vue?vue&type=template&id=d182fc18&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DealsForm_vue_vue_type_template_id_d182fc18___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DealsForm_vue_vue_type_template_id_d182fc18___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);