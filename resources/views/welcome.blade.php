@extends('layouts.app')

@section('content')
    @component('components.full-page-section')
        @component('components.card')
            @slot('title')
                <span class="icon"><i class="mdi mdi-laravel"></i></span>
                <span>Online Ordering</span>
            @endslot

            <div class="content">
                <p>
                    Use our free restaurant ordering software to drive your online sales. No matter how much your business grows, you'll always benefit from taking unlimited orders with zero fees. There's no other our alternative that can help you power your business like our free online ordering system for restaurants.
                </p>

            </div>
            <hr>
            <div class="buttons">
                <a href="{{ route('login') }}" class="button is-black">Login</a>
                <a href="{{ route('register') }}" class="button is-black is-outlined">Register</a>
            </div>
        @endcomponent
    @endcomponent
@endsection