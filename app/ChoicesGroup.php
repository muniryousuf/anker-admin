<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChoicesGroup extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $table = "choices_group";

    protected $fillable = [
        'name',
        'type',
        'display_type'
    ];

    protected $with = ['choices'];

    public function choices() {
        return $this->hasMany('App\Choices','id_group','id') ;
    }

}
