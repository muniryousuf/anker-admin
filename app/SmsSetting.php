<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsSetting extends Model
{

    protected $primaryKey = 'customer_id';
    protected $table ="sms_setting";
    protected $fillable = ['customer_id','key','secret','gateway_key'];
}
