<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoucherStoreRequest;
use App\Voucher;
use Illuminate\Http\Request;

class VouchersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {
        $vouchers = Voucher::get();

        return response()->json([
            'data' => $vouchers
        ]);
    }

    /**
     * Get single resource
     *
     * @param Vouchers $Vouchers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Voucher $voucher ) {

        return response()->json([
            'data' => $voucher
        ]);
    }

    /**
     * Update single resource
     *
     * @param VoucherStoreRequest $request
     * @param Voucher $Voucher
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( VoucherStoreRequest $request, Voucher $voucher ) {
        $voucher->fill($request->all());
        $voucher->save();

        return response()->json([
            'status' => true,
            'data' => $voucher
        ]);
    }

    /**
     * Store new resource
     *
     * @param VoucherStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( VoucherStoreRequest $request ) {
        $voucher = new Voucher;

        $voucher->fill($request->all());
        $voucher->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $voucher->id
            ]
        ]);
    }

    /**
     * Destroy single resource
     *
     * @param Voucher $Voucher
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy( Voucher $voucher ) {
        $voucher->delete();

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * Destroy resources by ids
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroyMass( Request $request ) {
        $request->validate([
            'ids' => 'required|array'
        ]);

        Voucher::destroy($request->ids);

        return response()->json([
            'status' => true
        ]);
    }
}
