<?php

namespace App\Http\Controllers;

use App\MessageTemplating;
use Illuminate\Http\Request;

class MessageTemplatingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request) {
        $messageTemplating =  MessageTemplating::all();
        return response()->json(['data' => $messageTemplating]);
    }

    public function show( MessageTemplating $messageTemplating) {

        return response()->json([
            'data' => $messageTemplating
        ]);
    }

    public function store(Request $request ) {

        $requestData = $request->all();
        $messageTemplating = new  MessageTemplating();
        $messageTemplating->fill($request->all());
        $messageTemplating->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $messageTemplating->id
            ]
        ]);

    }

    public function update(Request $request,  MessageTemplating $messageTemplating ) {
        $requestData = $request->all();


        $messageTemplating->fill($request->all());
        $messageTemplating->save();
        return response()->json([
            'status' => true,
            'data' => $messageTemplating
        ]);
    }

    public function destroy($id) {

        $messageTemplating =  MessageTemplating::find($id);

        if($messageTemplating) {
            $messageTemplating->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }
    public function destroyMass( Request $request ) {

        $request->validate([
            'id' => 'required|array'
        ]);

        MessageTemplating::destroy($request->id);

        return response()->json([
            'status' => true
        ]);
    }
}
