<?php

namespace App\Http\Controllers;

use App\OurStory;
use Illuminate\Http\Request;

class OurStoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request) {



        $ourStory = OurStory::all();
        return response()->json(['data' => $ourStory]);
    }

    public function show(OurStory $ourStory) {

        return response()->json([
            'data' => $ourStory
        ]);
    }

    public function store(Request $request ) {

        $requestData = $request->all();

        $checkData = OurStory::get()->toArray();
        if (empty($checkData)) {


            $requestData = $request->all();

            if($requestData['image'] != null) {
                $imageName = time().'.'.$request->image->getClientOriginalExtension();
                $requestData['image'] = $imageName;
                $request->image->move(public_path('images/ourStory'), $imageName);
            }

            $ourStory = new OurStory();
            $ourStory->fill($requestData);
            $ourStory->save();



            return response()->json([
                'status' => true,
                'created' => true,
                'data' => [
                    'id' => $ourStory->id
                ]
            ]);
        }
    }

    public function update(Request $request, OurStory $ourStory ) {
        $requestData = $request->all();

        $requestData = $request->all();

        if($requestData['image'] != null) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $requestData['image'] = $imageName;
            $request->image->move(public_path('images/ourStory'), $imageName);
        }


        $ourStory->fill($requestData);
        $ourStory->save();
        return response()->json([
            'status' => true,
            'data' => $ourStory
        ]);
    }

    public function destroy($id) {

        $ourStory = OurStory::find($id);

        if($ourStory) {
            $ourStory->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }
    public function destroyMass( Request $request ) {

        $request->validate([
            'id' => 'required|array'
        ]);

        OurStory::destroy($request->id);

        return response()->json([
            'status' => true
        ]);
    }
}
