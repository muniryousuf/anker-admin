<?php

namespace App\Http\Controllers;

use App\CMS;
use Illuminate\Http\Request;

class CMSController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request) {
        $ourStory = CMS::all();
        return response()->json(['data' => $ourStory]);
    }

    public function show(CMS $cms) {

        return response()->json([
            'data' => $cms
        ]);
    }

    public function store(Request $request ) {

        $requestData = $request->all();
        $cms = new CMS();
        $cms->fill($request->all());
        $cms->save();

            return response()->json([
                'status' => true,
                'created' => true,
                'data' => [
                    'id' => $cms->id
                ]
            ]);

    }

    public function update(Request $request, CMS $cms ) {
        $requestData = $request->all();


        $cms->fill($request->all());
        $cms->save();
        return response()->json([
            'status' => true,
            'data' => $cms
        ]);
    }

    public function destroy($id) {

        $cms = CMS::find($id);

        if($cms) {
            $cms->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }
    public function destroyMass( Request $request ) {

        $request->validate([
            'id' => 'required|array'
        ]);

        CMS::destroy($request->id);

        return response()->json([
            'status' => true
        ]);
    }
}
