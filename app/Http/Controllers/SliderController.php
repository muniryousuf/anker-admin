<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderStoreRequest;
use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

        $slider = Slider::all();
        return response()->json(['data' => $slider]);
    }

    public function show(Slider $slider ) {

        return response()->json([
            'data' => $slider
        ]);
    }


    public function store(SliderStoreRequest $request ) {


        $requestData = $request->all();

        if($requestData['image'] != null) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $requestData['image'] = $imageName;
            $request->image->move(public_path('images/category'), $imageName);
        }

        $slider = new Slider();
        $slider->fill($requestData);
        $slider->save();


        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $slider->id
            ]
        ]);
    }

    public function update(Request $request, Slider $slider ) {



        $requestData = $request->all();

        if($requestData['image'] != null) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $requestData['image'] = $imageName;
            $request->image->move(public_path('images/category'), $imageName);
        }


        $slider->fill($requestData);
        $slider->save();

        return response()->json([
            'status' => true,
            'data' => $slider
        ]);
    }


    public function destroy($id) {

        $slider = Slider::find($id);

        if($slider) {
            $slider->delete();
        }

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }

    public function destroyMass( Request $request ) {
        $request->validate([
            'ids' => 'required|array'
        ]);

        Slider::destroy($request->ids);

        return response()->json([
            'status' => true
        ]);
    }
}
