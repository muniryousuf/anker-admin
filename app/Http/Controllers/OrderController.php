<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Orders;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function getAllOrders(Request $request)
    {
        $orders = Orders::orderby('id','Desc')->get();

        $output = [
            'data' => $orders,
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => "Orders Retrieved Successfully",
        ];
        return response()->json($output, Response::HTTP_OK);
    }

    public function updateOrderStatus(Request $request, $id)
    {
        $requestData = $request->all();

        $requestData['id'] = $id;

        $order = Orders::where('id', $id)->update(['status' => $requestData['status']]);

        $output = ['data' => $order, 'message' => "your order status has been updated successfully"];
        return response()->json($output, Response::HTTP_OK);
    }

    public function placeOrder(Request $request)
    {
        $requestData = $request->all();

        $validator =  Validator::make($requestData, [
            'user_id' => 'required',
            'total_amount_with_fee' => 'required',
            'delivery_fees' => 'required',
            'payment' => 'required',
            'delivery_address' => 'required',
            'order_details' => 'required|array',
            'order_details.*.price' => 'required|numeric',
            'order_details.*.product_id' => 'required|numeric',
            'order_details.*.product_name' => 'required',
            'order_details.*.quantity' => 'required'
        ]);

        if ($validator->fails()) {
            $code = 401;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        $data = $this->_repository->placeOrder($requestData);

        if ($data) {
            $this->sendNotification($requestData);
        }

        $output = ['data' => $requestData, 'message' => "your order has been placed successfully "];
        return response()->json($output, Response::HTTP_OK);
    }

    /** Send Push Notification */
    public function sendNotification($data)
    {
        $devices = UserDevices::get()->pluck('device_token')->toArray();

        $push = new PushNotification('fcm');

        $push->setMessage([
            'notification' => [
                'title' => 'This is the title',
                'body'=>'New order has been placed to your restaurant',
                'sound' => 'default'
            ],
        ])->setDevicesToken($devices)->send();

        $response = $push->getFeedback();
    }

    public function show($id) {

        $order = Orders::where('id', $id)->first();

        return response()->json(['data' => $order]);
    }
}
