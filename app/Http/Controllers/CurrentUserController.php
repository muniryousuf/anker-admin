<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordUpdateRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CurrentUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get current user's profile
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( Request $request ) {
        $user = $request->user();

        $user->load('file');
        $user->append('avatar');

        return response()->json([
            'data' => $request->user()
        ]);
    }

    /**
     * Update current user's profile
     *
     * @param ProfileUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProfileUpdateRequest $request ) {

        $user = $request->user();
        $user->fill($request->only(['name','email', 'phone_number']));
        $user->save();

        if($request->restaurant != null) {
            if($request->address['id']) {
                $user->address->fill(['zip_code' => $request->address['zip_code'], 'city' => $request->address['city'], 'address' => $request->address['address'], 'id_country' => $request->address['id_country']]);
                $user->address->save();
            } else {
                UserAddress::create(['id_user' => $user->id, 'zip_code' => $request->address['zip_code'], 'city' => $request->address['city'], 'address' => $request->address['address'], 'id_country' => $request->address['id_country']]);
            }
        }

        $user->append('avatar');

        return response()->json([
            'data' => $user
        ]);
    }

    /**
     * Update current user's password
     *
     * @param PasswordUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword( PasswordUpdateRequest $request ) {
        $user = $request->user();
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'status' => true
        ]);
    }
}
