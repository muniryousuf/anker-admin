<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryStoreRequest;
use App\Product;
use App\ProductGroups;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Index resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {

        $categories = Category::all();
        return response()->json(['data' => $categories]);
    }

    /**
     * Store new resource
     *
     * @param CategoryStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryStoreRequest $request ) {

        $requestData = $request->all();

        if($requestData['image'] != null) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $requestData['image'] = $imageName;
            $request->image->move(public_path('images/category'), $imageName);
        }

        $category = new Category();
        $category->fill($requestData);
        $category->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $category->id
            ]
        ]);
    }

    /**
     * Store new resource
     *
     * @param CategoryDestroyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        $category = Category::find($id);

        $products = $category->products();

        foreach($products->get() as $product) {
            $product->productGroups()->delete();
            $product->productSizes()->delete();
        }

        $products->delete();

        $category->delete();

        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }
}
