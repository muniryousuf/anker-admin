<?php

namespace App\Http\Controllers;

use App\Choices;
use App\Http\Requests\ChoicesStoreRequest;

class ChoicesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store new resource
     *
     * @param ChoicesRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ChoicesStoreRequest $request ) {
        $product = new Choices();
        $product->fill($request->all());
        $product->save();

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $product->id
            ]
        ]);
    }
}
