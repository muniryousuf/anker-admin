<?php

namespace App\Http\Controllers;

use App\DeliveryCharges;
use App\DeliveryChargesDetails;
use App\Http\Requests\DeliveryChargesRequest;
use Illuminate\Http\Request;

class DeliveryChargesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {


        $deliveryCharges = DeliveryCharges::select('*')
        ->join('delivery_charges_details','delivery_charges.id','=','delivery_charges_details.delivery_id')
                    ->get();

        return response()->json(['data' => $deliveryCharges]);
    }

    public function show($deliveryCharges) {
      $id = $deliveryCharges;
        $deliveryCharges = DeliveryCharges::select('*')
            ->join('delivery_charges_details','delivery_charges.id','=','delivery_charges_details.delivery_id')
            ->where('delivery_charges_details.id','=',$id)
            ->get()->toArray();
        return response()->json([
            'data' => $deliveryCharges
        ]);
    }

    public function store(DeliveryChargesRequest $request ) {

        $input = $request->all();
        $delivery = new DeliveryCharges();
        $delivery->delivery_types = $request->delivery_types;
        $delivery->save();
        $deliveryCharges = new DeliveryChargesDetails();
        $deliveryCharges->fill($request->all());

        $delivery->deliveryChargesDetails()->save($deliveryCharges);

        return response()->json([
            'status' => true,
            'created' => true,
            'data' => [
                'id' => $delivery->id
            ]
        ]);
    }

    public function update(Request $request,  $deliveryCharges ) {

         DeliveryCharges::select('*')
            ->join('delivery_charges_details','delivery_charges.id','=','delivery_charges_details.delivery_id')
            ->where('delivery_charges_details.id','=',$deliveryCharges)
            ->delete();
        $delivery = new DeliveryCharges();
        $delivery->delivery_types = $request->delivery_types;
        $delivery->save();
        $deliveryCharges = new DeliveryChargesDetails();
        $deliveryCharges->fill($request->all());

        $delivery->deliveryChargesDetails()->save($deliveryCharges);


        return response()->json([
            'status' => true,
            'data' => $deliveryCharges

        ]);
    }

    public function destroy($id) {

        $deliveryCharges = $deliveryCharges = DeliveryCharges::select('*')
            ->join('delivery_charges_details','delivery_charges.id','=','delivery_charges_details.delivery_id')
            ->where('delivery_charges_details.id','=',$id)
            ->delete();


        return response()->json([
            'status' => true,
            'deleted' => true,
            'data' => []
        ]);
    }
    public function destroyMass( Request $request ) {

        $request->validate([
            'id' => 'required|array'
        ]);

         DeliveryCharges::select('*')
            ->join('delivery_charges_details','delivery_charges.id','=','delivery_charges_details.delivery_id')
            ->whereIn('delivery_charges_details.id',$request->id)
            ->delete();
        return response()->json([
            'status' => true
        ]);
    }
}
