<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralSettingsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'site_name' => 'required',
            'site_title' => 'required',
            'tag_line' => 'required',
            'header_logo' => 'required',
            'footer_logo' => 'required',
            'favicon' => 'required'
        ];

        return $rules;
    }
}
