<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class DealProducts extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'item_id',
        'quantity',
        'id_deal',
        'is_category',
        'is_options'
    ];

    protected $with = ['product'];

    public function product() {
        return $this->hasMany('App\Product','id','id_product') ;
    }

}
