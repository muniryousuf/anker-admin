<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class   Deal extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'name',
        'price',
        'status',
        'description'
    ];

    protected $with = ['products'];

    public function products() {
        return $this->hasMany('App\DealProducts','id_deal','id') ;
    }
}
