<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryCharges extends Model
{
    protected $table ="delivery_charges";
    protected $fillable = ['delivery_type'];

    public function deliveryChargesDetails()
    {
        return $this->hasMany(DeliveryChargesDetails::class,'delivery_id','id');
    }
}
