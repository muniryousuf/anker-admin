<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'name',
        'type',
        'coupon_limit',
        'discount',
        'expiry_date',
        'used_once',
        'status'
    ];

    protected $appends = [
        'Expiry'
    ];

    public function getExpiryAttribute() {
        if (empty($this->expiry_date)) {
            return null;
        }

        $expiry = Carbon::parse($this->expiry_date);

        return $expiry->format('M d Y');
    }
}
