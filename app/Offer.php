<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'percentage',
        'orders_over',
        'valid_from',
        'valid_to',
        'used_once',
        'status'
    ];

    protected $appends = [
        'From', 'To'
    ];

    public function getFromAttribute() {
        if (empty($this->valid_from)) {
            return null;
        }

        $from = Carbon::parse($this->valid_from);

        return $from->format('M d Y');
    }

    public function getToAttribute() {
        if (empty($this->valid_to)) {
            return null;
        }

        $to = Carbon::parse($this->valid_to);

        return $to->format('M d Y');
    }
}
